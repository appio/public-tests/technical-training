import type { NextApiRequest, NextApiResponse } from "next";

export default (req: NextApiRequest, res: NextApiResponse) => {
  if (req.method !== "POST") {
    res.status(405);
  }

  res.setHeader("set-cookie", "token=1234token56789;Max-Age=2592000;HttpOnly");
  res.status(200).json({ success: true });
};
