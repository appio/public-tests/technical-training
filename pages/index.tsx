import { x } from "@xstyled/emotion";
import { Container } from "anolis-ui";

import type { NextPage } from "next";

const Home: NextPage = () => {
  return (
    <Container>
      <x.h1 fontSize="lg">Technical training calendar</x.h1>
      {/* TODO: create event training list */}
    </Container>
  );
};

export default Home;
