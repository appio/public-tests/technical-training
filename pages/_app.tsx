import { Preflight, ThemeProvider, defaultTheme } from "@xstyled/emotion";
import type { AppProps } from "next/app";
import { AnolisProvider, createTheme } from "anolis-ui";
import { FC } from "react";

const MyApp: FC<AppProps> = ({ Component, pageProps }) => {
  return (
    <ThemeProvider theme={defaultTheme}>
      <AnolisProvider theme={theme}>
        <Preflight />
        <Component {...pageProps} />
      </AnolisProvider>
    </ThemeProvider>
  );
};

export default MyApp;

const theme = createTheme();
